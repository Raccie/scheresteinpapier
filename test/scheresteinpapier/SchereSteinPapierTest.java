/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scheresteinpapier;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Simon
 */
public class SchereSteinPapierTest {
    /**
     * Funktionstest SchereSteinPapier.toString gibt "Schere" korrekt aus
     */
    @Test
    public void testToStringSchere() {
        //Arrange
        SchereSteinPapier SSP = new SchereSteinPapier();                
        //Act
        String out = SSP.toString(0);
        //Assert
        assertEquals("Auswahl", out, "Schere");        
    }
    /**
     * Funktionstest SchereSteinPapier.toString gibt "Stein" korrekt aus
     */
    @Test
    public void testToStringStein() {
        //Arrange
        SchereSteinPapier SSP = new SchereSteinPapier();                
        //Act
        String out = SSP.toString(1);
        //Assert
        assertEquals("Auswahl", out, "Stein");        
    }
    /**
     * Funktionstest SchereSteinPapier.toString gibt "Papier" korrekt aus
     */
    @Test
    public void testToStringPapier() {
        //Arrange
        SchereSteinPapier SSP = new SchereSteinPapier();                
        //Act
        String out = SSP.toString(2);
        //Assert
        assertEquals("Auswahl", out, "Papier");        
    }
    
    /**
     * Funktionstest SchereSteinPapier.werGewinnt gibt korrekte Ergebnisse aus
     */
    @Test
    public void testWerGewinnt1() {
        //Arrange
        int Spieler0Zug = 0;
        int Spieler1Zug = 1;
        SchereSteinPapier SSP = new SchereSteinPapier();
        //Act
        int gewinner = SSP.werGewinnt(Spieler0Zug, Spieler1Zug);
        //Assert
        assertEquals("Gewinner", 1, gewinner);
    }
    /**
     * Funktionstest SchereSteinPapier.werGewinnt gibt korrekte Ergebnisse aus
     */
    @Test
    public void testWerGewinnt2() {
        //Arrange
        int Spieler0Zug = 1;
        int Spieler1Zug = 2;
        SchereSteinPapier SSP = new SchereSteinPapier();
        //Act
        int gewinner = SSP.werGewinnt(Spieler0Zug, Spieler1Zug);
        //Assert
        assertEquals("Gewinner", 1, gewinner);
    }
    /**
     * Funktionstest SchereSteinPapier.werGewinnt gibt korrekte Ergebnisse aus
     */
    @Test
    public void testWerGewinnt3() {
        //Arrange
        int Spieler0Zug = 0;
        int Spieler1Zug = 2;
        SchereSteinPapier SSP = new SchereSteinPapier();
        //Act
        int gewinner = SSP.werGewinnt(Spieler0Zug, Spieler1Zug);
        //Assert
        assertEquals("Gewinner", 0, gewinner);
    }
    /**
     * Funktionstest SchereSteinPapier.werGewinnt gibt korrekte Ergebnisse aus
     */
    @Test
    public void testWerGewinnt4() {
        //Arrange
        int Spieler0Zug = 2;
        int Spieler1Zug = 0;
        SchereSteinPapier SSP = new SchereSteinPapier();
        //Act
        int gewinner = SSP.werGewinnt(Spieler0Zug, Spieler1Zug);
        //Assert
        assertEquals("Gewinner", 1, gewinner);
    }
    
}
