package scheresteinpapier;

import java.util.Random;
import javax.swing.*;

public class SchereSteinPapierGUI {
    SchereSteinPapier SSP = new SchereSteinPapier();
    Punktestand punktestand = new Punktestand();
    boolean ersteRunde = true;
    
    int auswahl = 0;
    int auswahlCPU = 0;
    int gewinner = 0;
   
    public void Spiele(){      
        while (true){
            auswahl = GetUserInput();
            HandleInput(auswahl);
            ersteRunde = false;
        }
    }
    private int GetUserInput(){
        if(ersteRunde){
        Object[] options = {"Schere", "Stein", "Papier", "Neu starten", "Abbrechen"};
        return JOptionPane.showOptionDialog(null,
                "Was wählen Sie?",
                "Schere Stein Papier",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        }
        else{
        Object[] options = {"Schere", "Stein", "Papier", "Neu starten", "Abbrechen"};
        return JOptionPane.showOptionDialog(null,
                String.format(
                        "Spieler spielt: %s\nComputer spielt: %s\n"
                      + "Gewinner: %s\n\nSpielstand:\n"
                      + "  Spieler: %s\n  Computer: %s\n\n"
                      + "Was wählen Sie?", SSP.toString(auswahl), SSP.toString(auswahlCPU), spielerToString(gewinner),
                      punktestand.getStandSpieler0(), punktestand.getStandSpieler1()),
                "Schere Stein Papier",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        }
    }

    private int HandleInput(int input){
        switch (input){
            case 3: //restart Game
                punktestand.zuruecksetzen();
                ersteRunde = true;
                break;
            case 4: //exit Game
            case -1:
                System.exit(0);
                break;
            case 0:
            case 1:
            case 2:
                auswahlCPU = new Random().nextInt(3);
                gewinner = SSP.werGewinnt(input, auswahlCPU);
                punktestand.gewinnFuer(gewinner);
                break;
            default:
                break;
        }
        return -1;
    }
    
    private String spielerToString(int spieler){
        if(spieler == -1) return "Niemand";
        if(spieler == 0) return "Spieler";
        else return "Computer";
    }
}
