package scheresteinpapier;

public class SchereSteinPapier {
    public static final int SCHERE = 0;
    public static final int STEIN = 1;
    public static final int PAPIER = 2;

    public int werGewinnt(int spieler0Zug, int spieler1Zug){
        if(0 <= spieler0Zug && spieler0Zug<= 3 && 0 <= spieler1Zug && spieler1Zug<= 3){
            if(spieler0Zug == spieler1Zug) return -1;
            if(spieler0Zug == 2 && spieler1Zug == 0) return 1;
            if(spieler1Zug == 2 && spieler0Zug == 0) return 0;
            if(spieler0Zug < spieler1Zug) return 1;            
            else return 0;
        }
        return -1;
    }

    public String toString(int schereSteinPapier) {
        if(schereSteinPapier == SCHERE) return "Schere";
        if(schereSteinPapier == STEIN) return "Stein";
        if(schereSteinPapier == PAPIER) return "Papier";
        else return "Fehler";
    }
}