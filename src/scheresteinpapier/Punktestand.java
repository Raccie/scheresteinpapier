package scheresteinpapier;

public class Punktestand {
    private int standSpieler0;
    private int standSpieler1;
    
    public void zuruecksetzen(){
        standSpieler0 = 0;
        standSpieler1 = 0;
    }
    
    public void gewinnFuer(int spieler){
        if(spieler == -1) return;
        if(spieler == 0) standSpieler0++;
        else standSpieler1++;
    }

    public int getStandSpieler0() {
        return standSpieler0;
    }

    public int getStandSpieler1() {
        return standSpieler1;
    }       
}
